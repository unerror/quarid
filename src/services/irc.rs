//! IRC service for interacting with underlying IRC server.
//!
//! #Examples
//!
//!     let irc_service = match IrcService::new(config::Config, logger::Logger) {
//!         Ok(val) => val,
//!         Err(err) => panic!("Cannot connect: {:?}", err)
//!     }
//!
//!     for msg in irc_service.iter() {
//!         match msg {
//!             Ok(val) => // do something with the message
//!             Err(err) => // has an Err(err)
//!         }
//!     }
extern crate irc as irclib;
extern crate slog;

use std::collections::HashMap;
use std::default::Default;
use std::clone::Clone;
use std::io;

use self::irclib::client::prelude::{Config as IrcConfig, IrcServer, Server, ServerExt};

use services::{Service, ServiceError, ServiceResult};
use services::bot::Message;
use config::Config as qConfig;
use logger::Logger;

/// `IrcService` represents an IRC service instance. [`Message`](bot/struct.Message.html)`s` are
/// logged on iter() to rhe associated logger
#[derive(Default, Clone)]
pub struct IrcService {
    connection: Option<IrcServer>,

    nickname: Option<String>,
    username: Option<String>,

    /// Channels are {"channel" => "passwsord"} in the config, or a HashMap<String, String>
    /// with the same data. Channels without password use an empty &str for their `password`
    /// field
    channels: Option<HashMap<String, String>>,

    server: Option<String>,
    port: Option<u16>,

    logger: Option<Logger>,
}
impl IrcService {
    /// Connect to an IRC server, and return a `ServiceResult with an Err if we're unable to connect
    ///
    /// # Error
    ///
    /// - `ServiceError::Config` - thrown during error parsing configuration
    /// - `ServiceError::Connect` - thrown during an error connecting (or server IDENT)
    fn connect(&mut self) -> ServiceResult<()> {
        let channels = self.channels.take().unwrap();

        // configuration for underlying IRC connection
        let config = IrcConfig {
            nickname: self.nickname.take(),
            username: self.username.take(),
            server: self.server.take(),
            channels: Some(channels.keys().map(|v| v.clone()).collect::<Vec<String>>()),
            channel_keys: Some(channels),
            port: Some(self.port.unwrap_or(6667)),
            use_ssl: if self.port.unwrap_or(6667) == 6697 {
                Some(true)
            } else {
                Some(false)
            },
            ..Default::default()
        };

        // Load configuration from above
        let server = match IrcServer::from_config(config) {
            Ok(server) => server,
            Err(err) => return Err(ServiceError::Config(err)),
        };

        // Sent IDENT and other otherwise identify

        match server.identify() {
            Err(err) => return Err(ServiceError::Connect(err)),
            _ => (),
        }

        self.connection = Some(server);

        Ok(())
    }
}

/// IrcServerIterator iterates over messages received from the IRC server, and additionally
/// logs those messages.
pub struct IrcServiceIterator<'a> {
    service: &'a IrcService,
    logger: Logger,
}

impl<'a> IrcServiceIterator<'a> {
    /// Create a new IrcServerIterator
    fn new(service: &'a IrcService, logger: Logger) -> IrcServiceIterator<'a> {
        IrcServiceIterator {
            service: service,
            logger: logger,
        }
    }
}

impl<'a> Iterator for IrcServiceIterator<'a> {
    type Item = Result<Message, ServiceError>;

    /// Iterate over IrcServerIterator messages from the underlying IRC connection, while
    /// logging to the [`IrcService`](struct.IrcService.html)'s [`Logger`](../logger/struct.Logger.html) field.
    fn next(&mut self) -> Option<Result<Message, ServiceError>> {
        // unwrap is OK here since we have a connection from IrcService::new(), and iter() on that
        // connection will reconnect on failures

        let server = match self.service.connection {
            Some(ref val) => val,
            None => return Some(Err(ServiceError::IrcError)),
        };

        for val in server.iter() {
            let ref logger = self.logger;
            match val {
                Ok(msg) => {
                    logger.clone().info(msg.to_string().as_str());
                    return Some(Ok(Message::from(msg))); // return our Message from irclib::Message
                }
                Err(_) => return Some(Err(ServiceError::IrcError)),
            };
        }

        None
    }
}

impl Service for IrcService {
    /// Create an new [`IrcBot`] instance, and returns `Result<Bot<IrcBot>>, BotError`.
    /// BotErrors that can happen are BotError::Config and BotError::Connect
    fn new(config: qConfig, logger: Logger) -> Result<Box<IrcService>, ServiceError> {
        // Load [irc] section from config
        let irc_keys = match config.irc {
            Some(val) => val,
            None => return Err(ServiceError::Config(io::Error::last_os_error())),
        };

        // Load `IrcBot` from config
        let mut bot = Box::new(IrcService {
            connection: None,

            nickname: Some(config.name.clone()),
            username: Some(config.name.clone()),

            channels: Some(irc_keys.channels.unwrap()),

            server: Some(irc_keys.server),
            port: Some(irc_keys.port),

            logger: Some(logger),
        });

        // Connect to IRC server
        match bot.connect() {
            Err(err) => return Err(err),
            Ok(_) => (),
        }

        Ok(bot)
    }

    /// Create and return a Boxed Iterator for each Message that we get from the underlying service
    /// so that we can map actions in our handlers
    fn iter<'b>(&'b self) -> Box<Iterator<Item = Result<Message, ServiceError>> + 'b> {
        let logger = match self.logger {
            Some(ref val) => val.clone(),
            None => Logger::new(),
        };

        Box::new(IrcServiceIterator::new(self, logger))
    }

    /// Send a message (PRIVMSG on IRC) to some `target: &str`
    fn msg(&self, target: &str, msg: &str) -> io::Result<()> {
        match self.connection {
            Some(ref conn) => conn.send_privmsg(target, msg),
            None => Err(io::Error::from(io::ErrorKind::ConnectionReset)),
        }
    }
}

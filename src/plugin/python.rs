//! Python plugin
//!
//! The Python plugin implements an embedded Python (CPython3) interpreter so that Quarid can use
//! Python in it's interactions

extern crate regex;

use std::env;
use std::error;
use std::fmt;
use std::sync::{Arc, Mutex};
use std::path::Path;

use plugin::{Plugin, PluginError, PluginResult};
use plugin::python_module::PyModule_quarid;

use pyo3::{AsPyRef, GILGuard, IntoPyDictPointer, IntoPyObject, IntoPyTuple, NoArgs, PyErr,
           PyObject, PyString, Python as CPython};
use pyo3::ffi;
use self::regex::RegexSet;

/// `Python` implements [`Plugin`](../struct.Plugin.html) for calling Python plugins
#[derive(Clone)]
pub struct Python {
    // GIL-protected python module imported when initailized
    pymod: Arc<Mutex<PyObject>>,
    filename: String,
}

/// PythonErrors are errors that occur during interacting the the embedded Python interpreter
#[derive(Debug)]
pub enum PythonError {
    ImportError,

    FatalError(PyErr),
}
impl fmt::Display for PythonError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PythonError::FatalError(_) => write!(f, "Fatal general error"),
            PythonError::ImportError => write!(f, "Plugin import failed"),
        }
    }
}
impl error::Error for PythonError {
    fn description(&self) -> &str {
        match *self {
            PythonError::FatalError(_) => "Fatal general error",
            PythonError::ImportError => "Failed to import the provided plugin",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            PythonError::FatalError(_) => None,
            PythonError::ImportError => None,
        }
    }
}
impl From<PyErr> for PythonError {
    fn from(err: PyErr) -> PythonError {
        return PythonError::FatalError(err);
    }
}

impl Python {
    /// Return a new `Python` [`Plugin`](../struct.Plugin.html)
    #[allow(unused_must_use)]
    pub fn new<S>(filename: S) -> Self
    where
        S: Into<String>,
    {
        let fname = filename.into().clone();
        let module_path = Path::new(fname.as_str());
        let import_path = format!(
            "import sys; sys.path.append('{}/plugins/python');",
            env::current_dir().unwrap().display()
        );

        let gil = Python::gil();
        let py = gil.python();

        match py.run(import_path.as_str(), None, None) {
            Ok(_) => (),
            Err(err) => {
                err.print(py);
            }
        }

        let pymod = match Python::_import(py, module_path) {
            Ok(val) => val,
            Err(PluginError::PythonError(err)) => {
                panic!("Unable to load module {:?}: {:?}", fname, err)
            }
            _ => panic!("Unknown error loading Python module for {:?}", fname),
        };

        Python {
            pymod: Arc::new(Mutex::new(pymod)),
            filename: fname.clone(),
        }
    }

    // Call the Plugin implementation
    fn _call_impl<S, A, K>(&self, fn_name: S, args: A, kwargs: K) -> PluginResult<String>
    where
        S: Into<String>,
        A: IntoPyTuple,
        K: IntoPyDictPointer,
    {
        let gil = Python::gil();
        let py = gil.python();

        let module_shared = self.pymod.clone();
        let module_guard = module_shared.as_ref().lock();

        match module_guard {
            Ok(m) => match m.call_method(py, fn_name.into().as_str(), args, kwargs) {
                Ok(val) => Ok(PyString::from_object(val.as_ref(py), "utf-8", "")
                    .unwrap()
                    .to_string()
                    .unwrap()
                    .into_owned()),
                Err(err) => Err(PluginError::CallError),
            },
            Err(err) => Err(PluginError::CallError),
        }
    }

    // Acquire the GIL, and set up internal modules for use
    fn gil() -> GILGuard {
        unsafe {
            ffi::PyImport_AppendInittab("quarid\0".as_ptr() as *const _, Some(PyModule_quarid));
        }

        GILGuard::acquire()
    }

    // Import a Python module by filepath (must be a Python module in the PYTHONPATH)
    fn _import<'b>(python: CPython, path: &'b Path) -> PluginResult<PyObject> {
        let module_path = match path.to_str() {
            Some(val) => val,
            None => return Err(PluginError::PythonError(PythonError::ImportError)),
        };

        let module = match python.import(module_path) {
            Ok(val) => val.into_object(python),
            Err(err) => {
                err.print(python);
                return Err(PluginError::PythonError(PythonError::ImportError));
            }
        };

        Ok(module)
    }
}

impl Plugin for Python {
    fn about(&self) -> &str {
        self.filename.as_str()
    }

    fn call<S>(&self, fn_name: S, args: Option<Vec<String>>) -> PluginResult<String>
    where
        S: Into<String>,
    {
        self._call_impl(fn_name.into(), NoArgs, NoArgs)
    }

    fn reload(&self) -> Self {
        Python::new(self.filename.clone())
    }

    fn match_regex(&self, msg: &str) -> PluginResult<()> {
        let plugin_match = self._call_impl("match", NoArgs, NoArgs)?;

        let match_set = match RegexSet::new(vec![plugin_match]) {
            Ok(val) => val,
            Err(err) => return Err(PluginError::MatchBuildError(err)),
        };

        if !match_set.is_match(msg) {
            return Err(PluginError::MatchError);
        }

        Ok(())
    }
}

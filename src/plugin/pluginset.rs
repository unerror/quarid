//! A [`PluginSet`](struct.PluginSet.html) is a collection of [`Plugin`](../trait.Plugin.html)`s`

use services::Service;
use services::bot;

use config;
use plugin::{Plugin, PluginError, PluginResult};
use plugin::python::Python;

use std::io;
use std::iter::Iterator;

/// `PluginSet` contains our plugins for each supported language. The languages are fields on the
/// struct as a `Vec<>` of that language.
#[derive(Clone)]
pub struct PluginSet {
    python: Vec<Python>,
}

impl PluginSet {
    /// Return a new `PluginSet`
    pub fn new() -> PluginSet {
        PluginSet { python: Vec::new() }
    }

    /// Load a `PluginSet` with a `Option<`[`config::Plugins`](../../config/struct.Config.html)`>`
    pub fn with_config(config: Option<config::Plugins>) -> PluginResult<PluginSet> {
        let mut set = PluginSet::new();

        let plugins = match config {
            Some(val) => val,
            None => return Err(PluginError::LoadError(io::Error::last_os_error())),
        };

        let py_plugins = match plugins.python {
            Some(val) => val,
            None => vec![],
        };
        set.python
            .extend(py_plugins.iter().map(|path| Python::new(path.as_str())));

        Ok(set)
    }

    /// Apply this `PluginSet` against the given [`Message`](../services/struct.Message.html`),
    /// and pass the given [`Service`](../services/trait.Service.html) and the running
    /// [`Bot`](../service/bot/struct.Bot.html).
    pub fn apply<'a, 'b, S: Service>(
        &self,
        service: S,
        bot: &'a bot::Bot,
        msg: &'b bot::Message,
    ) -> Vec<PluginResult<String>> {
        let resp = self.python
            .iter()
            .map(|p| match p.match_regex("^test$") {
                Ok(_) => match p.call("quarid", None) {
                    Ok(val) => Ok(val),
                    Err(err) => return Err(err),
                },

                Err(err) => match err {
                    PluginError::MatchError => Err(err),
                    PluginError::MatchBuildError(_) => Err(err),
                    _ => Err(PluginError::ApplyError(err.to_string())),
                },
            })
            .collect();

        resp
    }

    #[allow(dead_code)]
    /// Reload the `Python` plugin
    // TODO(enmand): Make this work
    pub fn reload(&mut self) {
        let python = self.python.clone();
        for (i, _) in python.iter().enumerate() {
            let element = self.python.remove(i).clone();
            self.python.push(element)
        }
    }
}
impl Default for PluginSet {
    fn default() -> Self {
        PluginSet::new()
    }
}

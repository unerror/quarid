//! Logger is an slog based log wrapped
extern crate slog_term;

use slog;
use slog::Drain;
use std::io;

/// `Logger` is a thin wrapper over `slog::Logger`
#[derive(Clone)]
pub struct Logger {
    logger: slog::Logger,
}

/// Represents a `Logger` instance, which is a logger using slog::Logger
impl Logger {
    /// Return a new logger, with cloned slog::Logger,
    pub fn new() -> Logger {
        let log = &Logger::slog();

        Logger {
            logger: log.clone(),
        }
    }

    /// Log as debug message
    #[allow(dead_code)]
    pub fn debug(&self, msg: &str) {
        slog_log!(self.logger, slog::Level::Debug, "debug", "{}", msg);
    }

    /// Log as info mesage
    #[allow(dead_code)]
    pub fn info(&self, msg: &str) {
        slog_log!(self.logger, slog::Level::Debug, "debug", "{}", msg);
    }

    /// Log as warn message
    #[allow(dead_code)]
    pub fn warn(&self, msg: &str) {
        slog_log!(self.logger, slog::Level::Debug, "debug", "{}", msg);
    }

    /// Log as error message
    #[allow(dead_code)]
    pub fn error(&self, msg: &str) {
        slog_log!(self.logger, slog::Level::Error, "debug", "{}", msg);
    }

    /// Creates an slog::Logger,
    fn slog() -> slog::Logger {
        let plain = slog_term::PlainSyncDecorator::new(io::stdout());

        slog::Logger::root(slog_term::FullFormat::new(plain).build().fuse(), slog_o!())
    }
}

//! quarid is an IoT bot, that will take actions based on some signal Message from a service.
//! Currently, the following services are supported:
//!
//!     - IRC
//!
//! Plugins are written in either Rust, or one of the supported languages. Currently, the following
//! languages are supported:
//!
//!     - Python
//!
//! # Services
//!
//! A service is something that Quarid will interact with in some way. Some examples of this include
//!
//! - an IRC server
//! - a webserver (via. request, or webhook)
//! - an IoT device
//! - a queue
//!
//! Quarid listens on those services for Messages, and responds either on the service, or across
//! all services, or by taking some other action.
//!
//! ## Messages
//!
//! Messages are sent from a service, to a plugin. The plugin engine will match the given messages
//! and run any associated code.
//!
//! # Plugins
//!
//! Plugins are used for interacting with services, or taking some action based on a message from
//! that service
#![feature(proc_macro, specialization, const_fn)]
extern crate pyo3;

#[macro_use]
extern crate serde_derive;
#[macro_use(slog_o, slog_b, slog_log, slog_record, slog_record_static, slog_kv)]
extern crate slog;

pub mod services;
pub mod config;
pub mod logger;
pub mod plugin;

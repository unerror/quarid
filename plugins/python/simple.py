import quarid as bot

def matched(fn):
    print("matched %s" % repr(fn))
    def decorate():
        return fn()

    return decorate

@matched
def match():
    print("matched")
    return "^test$"

@matched
def quarid():
    print("called!")
    return bot

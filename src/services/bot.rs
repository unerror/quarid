//! A [`Bot`](struct.Bot.html) is a main unit of interaction in Quarid. Bots have a
//! [`PluginSet`](../../plugin/struct.PluginSet.html) and
//! `Vec<`[`Service`](../trait.Service.html)`>`s fields, for interacting via
//! [`Plugin`](../../plugin/trait.Plugin.html)s to those services.
//!
//! # Example
//!
//!     Bot::new()
//!          .with_logger(logger)
//!          .with_irc_service(irc_service)
//!          .with_plugins(plugin_set)
//!          .handle()
//!
extern crate irc as irclib;

use self::irclib::client::data::{Command as IrcCommand, Message as IrcMessage};

use services::Service;
use plugin::PluginResult;
use plugin::pluginset::PluginSet;
use logger::Logger;
use services::irc::IrcService;

/// Command is part of a [`Message`](struct.Message.html) that identifies the
#[derive(Debug, Clone)]
pub enum Command {
    Message(String, String), // target, msg
    Unknown,
}
impl From<IrcCommand> for Command {
    fn from(cmd: IrcCommand) -> Command {
        match cmd {
            IrcCommand::PRIVMSG(target, msg) => Command::Message(target, msg),
            _ => Command::Unknown,
        }
    }
}

/// Message is a unit of information from an IoT device or connected service. The message contains
/// some information about the request (whatever that means in the context of the Service), which
/// can be acted on by [`Plugin`](../../plugin/trait.Plugin.html)s.
#[derive(Clone)]
pub struct Message {
    /// Prefix information for the Message
    pub prefix: String,

    /// Message Command type
    pub command: Command,
}
// Convert messages from the IRC Service to Quarid messages
impl From<IrcMessage> for Message {
    fn from(msg: IrcMessage) -> Message {
        Message {
            prefix: msg.prefix.unwrap(),
            command: Command::from(msg.command),
        }
    }
}
// Impl handle has default for handling messages (i.e. forwarding them to plugins)
impl Message {
    /// Default handle for handling a [`Message`](struct.Message.html`). By default, this forwards
    /// the message to each [`Plugin`](../../plugin/trait.Plugin.html), and includes the
    /// [`Service`](../trait.Service.html), the [`Bot`](struct.Bot.html) and a
    /// `Vec<`[`PluginResult`](../../plugin/type.PluginResult.html)`<String>>`
    pub fn handle<'a, S: Service>(
        &self,
        service: S,
        bot: &'a Bot,
        plugins: PluginSet,
    ) -> Vec<PluginResult<String>> {
        plugins.apply(service, bot, self)
    }
}

/// Bot represents a collection of [`Plugin`](../plugin/struct.Plugin.html)s (through the
/// [`PluginSet`](../plugin/struct.PluginSet.html) struct), and a collection of services (through
/// various fields on `Bot`.
pub struct Bot {
    plugins: PluginSet,
    logger: Option<Logger>,

    irc: Vec<IrcService>,
}

impl Bot {
    /// Initialize a new empty `Bot` with no [`Plugin`](../plugin/struct.Plugin.html)s, and no
    /// services.
    pub fn new() -> Self {
        Bot {
            plugins: PluginSet::new(),
            logger: None,

            irc: Vec::new(),
        }
    }

    /// Own a logger in this `Bot`
    pub fn with_logger(&mut self, logger: Logger) -> &mut Self {
        self.logger = Some(logger);

        self
    }

    /// Own an IRC service in this `Bot`.
    pub fn with_irc_service(&mut self, service: IrcService) -> &mut Self {
        self.irc.push(service);

        self
    }

    /// Own a [`PluginSet`](../plugin/struct.PluginSet.html`) in this `Bot`.
    pub fn with_plugins(&mut self, plugins: PluginSet) -> &mut Self {
        self.plugins = plugins;

        self
    }

    /// Handle the connection and any incoming [`Message`](bot/struct.Message.html) notifications
    /// from the server
    pub fn handle(&self) {
        for service in self.irc.iter().cloned() {
            for req in service.iter() {
                let bot = self.clone();

                let local_service = service.clone();
                let plugins = self.plugins.clone();

                match req {
                    Ok(msg) => msg.handle(local_service, bot, plugins),
                    Err(_) => vec![],
                };
            }
        }
    }
}

# Quarid IoT Bot

Quarid is an IoT bot that is capable of connecting to various services and performing actions based on messages from
those services.
 
# Requirements

- rustc 1.16.0+
- openssl development libraries
- Python 3 development libraries
 
## Building

To build Quarid, use the `cargo` command:

    cargo build --release
    
This will create the binary in `target/release/quaridd`.

## Running Quarid

Quarid can either be run either `cargo` (`cargo run bot start`), or using the compiled binary from above with
`quaridd bot start`.
//! Configuration provided to the Quarid bot
//!
extern crate serde;
extern crate toml;

use std::error;
use std::fmt;
use std::fs;
use std::io;
use std::io::Read;
use std::collections::HashMap;

/// ConfigError occurs when there was an issue loading or reading the configuration
#[derive(Debug)]
pub enum ConfigError {
    /// Read errors occur if we're unable to read the backing configuration (e.g. file)
    Read(io::Error),

    /// Load errors occur when we're unable to parse the TOML configuration
    Load(toml::de::Error),
}
impl fmt::Display for ConfigError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ConfigError::Read(ref err) => err.fmt(f),
            ConfigError::Load(ref err) => err.fmt(f),
        }
    }
}
impl error::Error for ConfigError {
    fn description(&self) -> &str {
        match *self {
            ConfigError::Read(ref err) => err.description(),
            ConfigError::Load(ref err) => err.description(),
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            ConfigError::Read(ref err) => Some(err),
            ConfigError::Load(ref err) => Some(err),
        }
    }
}
impl From<io::Error> for ConfigError {
    fn from(err: io::Error) -> ConfigError {
        ConfigError::Read(err)
    }
}
impl From<toml::de::Error> for ConfigError {
    fn from(err: toml::de::Error) -> ConfigError {
        ConfigError::Load(err)
    }
}

/// Config struct is a deserializable struct that contains configuration
// TODO(enmand): Allow multiple configurations in a Config, so that multiple bots can be loaded
// TODO(enmand): Allow multiple IRC (and other) services in a Bot
// TODO(enmand): Allow multiple Plugin types in a Bot
#[derive(Deserialize, Clone)]
pub struct Config {
    /// The name of the [`Bot`](service/bot/struct.Bot.html)
    pub name: String,

    /// The IRC server of
    pub irc: Option<IRC>,
    pub plugins: Option<Plugins>,
}

/// IRC configuration
#[derive(Deserialize, Clone)]
pub struct IRC {
    /// IRC server hostnmae
    pub server: String,
    /// IRC server port (6697 is SSL)
    pub port: u16,

    /// Channels (with keys) to join
    pub channels: Option<HashMap<String, String>>,
}

/// Plugin configuration
#[derive(Deserialize, Clone)]
pub struct Plugins {
    /// Python plugins
    pub python: Option<Vec<String>>,
}

impl Config {
    /// Read and load a new Config object from a configuration file
    pub fn new(filename: &str) -> Result<Config, ConfigError> {
        let mut cfg_buf = String::new();
        fs::File::open(filename).and_then(|mut f| f.read_to_string(&mut cfg_buf))?;
        let cfg = cfg_buf.as_str();

        match toml::from_str(cfg) {
            Ok(val) => Ok(val),
            Err(err) => Err(ConfigError::Load(err)),
        }
    }
}

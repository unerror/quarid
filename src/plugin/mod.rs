//! `Plugin` is a function, set of functions or module that interactions with Quarid. Plugins can
//! be written in various languages to interact with Quarid. Currently, Python is the only
//! implemented language

extern crate regex;

pub mod python;
pub mod python_module;
pub mod pluginset;

use plugin::python::PythonError;

use std::fmt;
use std::error::Error;
use std::io;

/// Plugin errors are errors that can occur during different stages of the `Plugin` interaction.
#[derive(Debug)]
pub enum PluginError {
    /// The plugin is not able to be loaded because of an IO problem
    LoadError(io::Error),

    /// The plugin match building failed to compiled the regular expression
    MatchBuildError(regex::Error),

    /// An error occurred when the [`Plugin`](trait.Plugin.html) is called
    CallError,

    /// An error occurred applying this plugin as part of a
    /// [`PluginSet`](pluginset/struct.PluginSet.html)
    ApplyError(String),

    /// An error occurred matching a [`Message`](../services/bot/struct.Message.htm)
    MatchError,

    /// An error occurred in the [`Python`] [`plugin`]
    PythonError(PythonError),
}
impl fmt::Display for PluginError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PluginError::LoadError(ref err) => err.fmt(f),
            PluginError::MatchBuildError(ref err) => err.fmt(f),
            PluginError::ApplyError(ref str) => str.fmt(f),
            PluginError::CallError => write!(f, "Call to plugin failed"),
            PluginError::MatchError => write!(f, "Plugin match failed"),

            PluginError::PythonError(ref err) => err.fmt(f),
        }
    }
}
impl Error for PluginError {
    fn description(&self) -> &str {
        match *self {
            PluginError::LoadError(ref err) => err.description(),
            PluginError::MatchBuildError(ref err) => err.description(),
            PluginError::ApplyError(ref str) => str,
            PluginError::CallError => "Call to plugin failed",
            PluginError::MatchError => "Plugin match failed",

            PluginError::PythonError(ref err) => err.description(),
        }
    }
    fn cause(&self) -> Option<&Error> {
        match *self {
            PluginError::LoadError(ref err) => Some(err),
            PluginError::MatchBuildError(ref err) => Some(err),
            PluginError::ApplyError(_) => None,
            PluginError::CallError => None,
            PluginError::MatchError => None,

            PluginError::PythonError(ref err) => Some(err),
        }
    }
}
impl From<io::Error> for PluginError {
    fn from(err: io::Error) -> PluginError {
        PluginError::LoadError(err)
    }
}
impl From<PythonError> for PluginError {
    fn from(err: PythonError) -> PluginError {
        PluginError::PythonError(err)
    }
}

// Type Aliases
/// Alias PluginResult for [`PluginError`][enum.PluginError.html)
pub type PluginResult<T> = Result<T, PluginError>;

/// Plugins are ways to interact with various other system languages within Quarid. A `Plugin` is
/// an association of functions (or some "module") that can be called directly.
pub trait Plugin {
    /// Information about this `Plugin`
    fn about(&self) -> &str;

    /// Call a function or method on this `Plugin`
    fn call<S: Into<String>>(&self, fn_name: S, args: Option<Vec<String>>) -> PluginResult<String>;

    /// Reload this `Plugin`
    fn reload(&self) -> Self;

    /// Compare this `Plugin` for a matching regular expression
    // TODO(enmand): Return PluginResult<String> with matching fn_name
    fn match_regex(&self, msg: &str) -> PluginResult<()>;
}

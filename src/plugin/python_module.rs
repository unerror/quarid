//! Python modules and types for Quarid interactions
use pyo3;
use pyo3::IntoPyPointer;
use pyo3::ffi;

use std::ptr;

/// A `quarid` module in Python
#[allow(non_snake_case)]
pub extern "C" fn PyModule_quarid() -> *mut ffi::PyObject {
    fn init(_py: pyo3::Python, m: &pyo3::PyModule) -> pyo3::PyResult<()> {
        m.add("__doc__", "Quarid Rust IoT Bot")?;
        Ok(())
    };

    #[no_mangle]
    unsafe {
        static mut MODULE_DEF: ffi::PyModuleDef = ffi::PyModuleDef_INIT;
        MODULE_DEF.m_name = "quarid\0".as_ptr() as *const _;

        pyo3::prepare_pyo3_library();

        #[cfg(py_sys_config = "WITH_THREAD")]
        ffi::PyEval_InitThreads();
        let _gil = pyo3::GILPool::new();
        let _py = pyo3::Python::assume_gil_acquired();

        let _module = ffi::PyModule_Create(&mut MODULE_DEF);
        if _module.is_null() {
            return _module;
        }
        let _module = match _py.from_owned_ptr_or_err::<pyo3::PyModule>(_module) {
            Ok(m) => m,
            Err(e) => {
                pyo3::PyErr::from(e).restore(_py);
                return ptr::null_mut();
            }
        };

        match init(_py, _module) {
            Ok(_) => _module.into_ptr(),
            Err(err) => {
                err.restore(_py);
                ptr::null_mut()
            }
        }
    }
}

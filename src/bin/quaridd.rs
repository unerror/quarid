#[macro_use]
extern crate clap;
extern crate quarid;

use quarid::plugin::pluginset::PluginSet;
use quarid::config::Config;
use quarid::services::Service;
use quarid::services::bot::Bot;
use quarid::services::irc::IrcService;
use quarid::logger::Logger;

use std::clone::Clone;
use std::error::Error;

use clap::{App, Arg, SubCommand};

arg_enum!{
    #[allow(non_camel_case_types)]
    #[derive(Debug)]
    /// Arguments for "bot" command
    pub enum BotArgs {
        start,
        stop
    }
}

fn main() {
    let matches = App::new("quarid-rust")
        .version("1.0")
        .author("Quarid Authors")
        .about("Quarid IoT Bot")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Configuration file")
                .takes_value(true),
        )
        .subcommand(
            SubCommand::with_name("bot")
                .about("Operates bot")
                .arg(Arg::with_name("status").possible_values(&BotArgs::variants())),
        )
        .get_matches();

    match matches.subcommand() {
        ("bot", Some(bot_matches)) => match value_t!(bot_matches, "status", BotArgs) {
            Ok(val) => {
                let config_file = matches.value_of("config").unwrap_or("config.toml");
                match val {
                    BotArgs::start => start_bot(config_file),
                    BotArgs::stop => {
                        bot_matches.usage();
                        ()
                    }
                }
            }
            Err(err) => {
                println!("{}", err);
                println!("{}", bot_matches.usage());
            }
        },
        _ => println!("{}", matches.usage()),
    };
    use std::iter::Iterator;
}

fn start_bot(config_file: &str) {
    let logger: Logger = Logger::new();
    let config = match Config::new(config_file) {
        Ok(val) => val,
        Err(err) => return logger.error(err.description()),
    };

    logger.debug("Loading plugins");
    let plugins = match PluginSet::with_config(config.clone().plugins) {
        Ok(val) => val,
        Err(err) => return logger.error(err.description()),
    };

    logger.debug("Starting IRC client");
    let irc_service = match IrcService::new(config, logger.clone()) {
        Ok(v) => v,
        Err(err) => return logger.error(err.description()),
    };

    logger.debug("Configuring Bot and related services");

    Bot::new()
        .with_logger(logger.clone())
        .with_plugins(plugins)
        .with_irc_service(*irc_service)
        .handle()
}

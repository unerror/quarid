//! Services are interactions between Quarid and some IoT device or third-party service. These
//! services send [`Message`](bot/struct.Message.html)`s` to Quarid, and also pass both the service that the [`Message`](bot/struct.Message.html) was sent on,
//! as well as the [`Bot`](bot/struct.Bot.html).
pub mod irc;
pub mod bot;

use std::fmt;
use std::error::Error;
use std::io;

use services::bot::Message;
use config::Config;
use logger::Logger;
use plugin::PluginError;

/// ServiceError defines errors related to the service as it's performing it's action.
#[derive(Debug)]
pub enum ServiceError {
    /// General failure in unknown area of service
    General,

    /// Failure in configuration (parsing, expected key missing, etc)
    Config(io::Error),

    /// Failure in the connection to the service. Sometimes this means that we were unable to
    /// connect to some streaming service (like IRC), otherwise it means we failed on the request
    /// (e.g. over HTTP)
    Connect(io::Error),

    /// The service tried to act on a plugin, but failed in some operation
    PluginError(PluginError),

    /// Service specific error
    IrcError,
}

// Trait impls for Display and Error
impl fmt::Display for ServiceError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ServiceError::General => write!(f, "General failure"),
            ServiceError::Config(ref err) => write!(f, "Configuration not valid: {:?}", err),
            ServiceError::Connect(ref err) => write!(f, "Connection error: {:?}", err),

            ServiceError::PluginError(ref err) => write!(f, "Plugin Failed: {:?}", err),
            ServiceError::IrcError => write!(f, "Error reading IRC server"),
        }
    }
}

impl Error for ServiceError {
    fn description(&self) -> &str {
        match *self {
            ServiceError::General => "General failure",
            ServiceError::Config(ref err) => err.description(),
            ServiceError::Connect(ref err) => err.description(),

            ServiceError::PluginError(ref err) => err.description(),
            ServiceError::IrcError => "IRC Read Error",
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            ServiceError::General => None,
            ServiceError::Config(ref err) => Some(err),
            ServiceError::Connect(ref err) => Some(err),

            ServiceError::PluginError(ref err) => Some(err),
            ServiceError::IrcError => None,
        }
    }
}
impl From<io::Error> for ServiceError {
    fn from(err: io::Error) -> ServiceError {
        match err.kind() {
            io::ErrorKind::TimedOut
            | io::ErrorKind::AddrInUse
            | io::ErrorKind::AddrNotAvailable
            | io::ErrorKind::BrokenPipe
            | io::ErrorKind::ConnectionAborted
            | io::ErrorKind::ConnectionRefused
            | io::ErrorKind::ConnectionReset
            | io::ErrorKind::NotConnected => ServiceError::Connect(err),
            io::ErrorKind::InvalidInput
            | io::ErrorKind::NotFound
            | io::ErrorKind::UnexpectedEof => ServiceError::Config(err),
            _ => ServiceError::General,
        }
    }
}
impl From<PluginError> for ServiceError {
    fn from(err: PluginError) -> ServiceError {
        ServiceError::PluginError(err)
    }
}

/// ServiceResult is the `Result<T>` type for [`ServiceError`](enum.ServiceError.html)`s`.
type ServiceResult<T> = Result<T, ServiceError>;

/// Services are interactions with Quarid with the outside world. This could be an IoT device, or
/// some other third-party service.
pub trait Service {
    /// Return a Box'ed Self, given a [`Logger`](../logger/struct.Logger.html) and a
    /// [`Config`](../config/struct.Config.html).
    fn new(config: Config, log: Logger) -> Result<Box<Self>, ServiceError>;

    /// Iterate over [`Message`](bot/struct.Message.html) that come from this `Service`
    fn iter<'b>(&'b self) -> Box<Iterator<Item = Result<Message, ServiceError>> + 'b>;

    /// Send a string-based `message` to a `target` on this `Service`
    fn msg(&self, target: &str, message: &str) -> io::Result<()>;
}
